#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <string.h>

#include "internal.h"
#include "cache.h"
#include "timespec.h"
#include "pkt_manager.h"
#include "nameserver.h"

typedef enum {
    STATELESS_NS_INITIALIZE,
    STATELESS_NS_UPDATE,
    STATELESS_NS_PROBING,
    STATELESS_NS_ACTIVE,
    STATELESS_NS_PASSIVE,
} StatelessNameserverState;

struct StatelessNameserver {
    StatelessServer* s;
    StatelessNameserverState state;

    char* domain_str;
    char* address_str;
    ldns_rdf* domain_rdf;
    ldns_rdf* address_rdf;

    /* Pending records (Initialization phase) */
    ldns_rr_list* pending;

    /* Cache */
    StatelessCache* cache;
};


/* Receive copies of owner/data --> cleanup */
static ldns_rr* generate_rr(ldns_rr_type type, ldns_rdf* owner, ldns_rdf* data, uint32_t ttl) {
    ldns_rr* rr;

    if (!owner) {
        return NULL;
    }
    if (!data) {
        ldns_rdf_free(owner);
        return NULL;
    }

    rr = ldns_rr_new();
    if (!rr) {
        ldns_rdf_free(data);
        ldns_rdf_free(owner);
        return NULL;
    }

    if (!ldns_rr_push_rdf(rr,data)) {
        ldns_rdf_free(data);
        ldns_rdf_free(owner);
        ldns_rr_free(rr);
        return NULL;
    }
    ldns_rr_set_owner(rr, owner);
    ldns_rr_set_type(rr, type);
    ldns_rr_set_ttl(rr, ttl);
    ldns_rr_set_class(rr, LDNS_RR_CLASS_IN);
    stateless_log_debug("generate_rr(%%r)", rr);
    return rr;
}

static StatelessStatus query_pkt(StatelessNameserver* ns, const char* query, ldns_rr_type type, uint16_t flags) {
    ldns_status status;
    ldns_pkt* pkt;

    assert(ns);
    assert(query);

    /* Query for existing ns */
    status = ldns_pkt_query_new_frm_str(&pkt, query, type, LDNS_RR_CLASS_IN, flags);
    if (status != LDNS_STATUS_OK) {
        return STATELESS_STATUS_NOMEM;
    }

    return stateless_server_send_query(ns->s, ns, pkt, NULL);
}

static int ipv4_to_hex(char* dst, size_t n, const char* address) {
    unsigned int a, b, c, d;
    int ret;

    assert(dst);
    assert(address);

    ret = sscanf(address, "%u.%u.%u.%u", &a, &b, &c, &d);
    if (ret < 4)
        return -1;

    ret = snprintf(dst, n, "%02x%02x%02x%02x", a, b, c, d);
    if (ret != 8)
        return -1;

    return 0;
}

static StatelessStatus query_t(StatelessNameserver* ns) {
    char query[LDNS_MAX_DOMAINLEN];
    char hex[10];

    assert(ns);

    if (ipv4_to_hex(hex, sizeof(hex), ns->address_str)) {
        return STATELESS_STATUS_ERROR;
    }
    stateless_log_debug("%s: %s -> %s", ns->domain_str, ns->address_str, hex);

    /* Generate nar query */
    snprintf(query, 1024, "%s.t100.%s", hex, ns->domain_str);

    return query_pkt(ns, query, LDNS_RR_TYPE_A, LDNS_RD|LDNS_CD);
}

StatelessNameserver* stateless_nameserver_new(StatelessServer* s, const char *domain, const char* address) {
    StatelessNameserver* ns;
    StatelessStatus status;

    assert(s);
    assert(domain);
    assert(address);

    stateless_log_info("nameserver_new(%s, %s)", domain, address);

    ns = calloc(sizeof(StatelessNameserver), 1);
    if (!ns) {
        return NULL;
    }
    ns->s = s;

    /* check argument validity through ldns */
    ns->domain_rdf = ldns_rdf_new_frm_str(LDNS_RDF_TYPE_DNAME, domain);
    if (!ns->domain_rdf || !(ns->domain_str = ldns_rdf2str(ns->domain_rdf))) {
        stateless_nameserver_free(&ns);
        return NULL;
    }
    ns->address_rdf = ldns_rdf_new_frm_str(LDNS_RDF_TYPE_A, address);
    if (!ns->address_rdf || !(ns->address_str = ldns_rdf2str(ns->address_rdf))) {
        stateless_nameserver_free(&ns);
        return NULL;
    }

    /* ns->saddr = ldns_rdf2native_sockaddr_storage(ns->addr, STATELESS_DNS_PORT, &ns->saddr_len); */

    /* Initilize pending queue */
    ns->pending = ldns_rr_list_new();
    if (!ns->pending) {
        stateless_nameserver_free(&ns);
        return NULL;
    }

    /* Initialize cache */
    ns->cache = stateless_cache_new(ns);
    if (!ns->cache) {
        stateless_nameserver_free(&ns);
        return NULL;
    }

    /* Query for existing NS. Stay in initial state until respose received */
    status = query_pkt(ns, ns->domain_str, LDNS_RR_TYPE_NS, LDNS_RD|LDNS_CD);
    if (status != STATELESS_STATUS_OK) {
        stateless_nameserver_free(&ns);
        return NULL;
    }
    ns->state = STATELESS_NS_INITIALIZE;

    return ns;
}

void stateless_nameserver_free(StatelessNameserver** ns) {
    StatelessNameserver* n;

    assert(ns);
    assert(*ns);

    n = *ns;

    if (n->domain_str) {
        free(n->domain_str);
    }
    if (n->address_str) {
        free(n->address_str);
    }
    if (n->domain_rdf) {
        ldns_rdf_free(n->domain_rdf);
    }
    if (n->address_rdf) {
        ldns_rdf_free(n->address_rdf);
    }
    if (n->pending) {
        ldns_rr_list_deep_free(n->pending);
    }
    if (n->cache) {
        /* TODO deep free? */
        stateless_cache_free(&n->cache);
    }

    free(n);
    *ns = NULL;
}

const char* stateless_nameserver_domain(const StatelessNameserver* ns) {
    assert(ns);
    return ns->domain_str;
}

static ldns_rr_list* generate_NS_A(const StatelessNameserver* ns) {
    char buffer[LDNS_MAX_DOMAINLEN];
    ldns_rr_list* list;
    ldns_rr* rr;
    ldns_rdf* delegation;

    assert(ns);

    /* Fill list with NS and A */
    list = ldns_rr_list_new();
    if (!list) {
        return NULL;
    }

    /* Generate delegation ns1 */
    snprintf(buffer, LDNS_MAX_DOMAINLEN, "ns1.%s", ns->domain_str);
    delegation = ldns_rdf_new_frm_str(LDNS_RDF_TYPE_DNAME, buffer);
    if (!delegation) {
        ldns_rr_list_free(list);
        return NULL;
    }

    /* NS */
    rr = generate_rr(LDNS_RR_TYPE_NS, ldns_rdf_clone(ns->domain_rdf), ldns_rdf_clone(delegation), 3600);
    if (!rr) {
        ldns_rr_list_free(list);
        ldns_rdf_free(delegation);
    }
    else if (!ldns_rr_list_push_rr(list, rr)) {
        ldns_rr_list_free(list);
        ldns_rdf_free(delegation);
        ldns_rr_free(rr);
        return NULL;
    }

    /* A */
    rr = generate_rr(LDNS_RR_TYPE_A, delegation, ldns_rdf_clone(ns->address_rdf), 3600);
    if (!rr) {
        ldns_rr_list_free(list);
    }
    else if (!ldns_rr_list_push_rr(list, rr)) {
        ldns_rr_list_deep_free(list);
        ldns_rr_free(rr);
        return NULL;
    }

    return list;
}

StatelessStatus stateless_nameserver_response(StatelessNameserver* ns, ldns_pkt* response) {
    int ancount;
    StatelessStatus status;
    ldns_rr_list* rr_list;
    ldns_rr_list* answers;
    const ldns_rr_list* c_rr_list;
    const ldns_rr* question;
    ldns_rdf* addr;
    ldns_pkt* update;

    assert(ns);
    assert(response);

    ancount = ldns_pkt_ancount(response);

    switch (ns->state) {
        case STATELESS_NS_INITIALIZE:
            if (ancount) {
                stateless_log_debug("NS(%s) found %d other ns", ns->domain_str, ancount);
                /* TODO check if own ip already in ns list */

                /* Save answers in cache */
                answers = ldns_pkt_answer(response);
                ldns_pkt_set_answer(response, NULL); // No pop implemented...
                status = stateless_cache_insert_list(ns->cache, answers);
                if (status != STATELESS_STATUS_OK) {
                    return status;
                }

                rr_list = ldns_pkt_additional(response);
                if (rr_list) {
                    /* TODO better selection */
                    addr = ldns_rr_rdf(ldns_rr_list_rr(rr_list, 0), 0);
                    ldns_pkt_set_additional(response, NULL);
                    status = stateless_cache_insert_list(ns->cache, rr_list);
                    if (status != STATELESS_STATUS_OK) {
                        return status;
                    }
                }
                else {
                    stateless_log_warn("TODO: NS(%s) receive ns without A", ns->domain_str);
                }

                /* Send update */
                rr_list = generate_NS_A(ns);
                if (!rr_list) {
                    return STATELESS_STATUS_NOMEM;
                }
                update = ldns_update_pkt_new(ldns_rdf_clone(ns->domain_rdf), LDNS_RR_CLASS_IN, NULL, rr_list, NULL);
                if (!update) {
                    ldns_rr_list_deep_free(rr_list);
                    return STATELESS_STATUS_NOMEM;
                }
                stateless_log_debug("NS(%s) Send update query", ns->domain_str);
                ns->state = STATELESS_NS_UPDATE;

                /* addr = ldns_rdf_new_frm_str(LDNS_RDF_TYPE_A, "10.0.0.11"); */
                return stateless_server_send_query(ns->s, ns, update, addr);
            }
            else {
                /* Register per nar query */
                stateless_log_info("NS(%s) Switch state probing", ns->domain_str);
                ns->state = STATELESS_NS_PROBING;
                return query_t(ns);
    /* ldns_rr_list* aa; */
    /* struct sockaddr_storage* saddr; */
    /* size_t saddr_len; */

            }
            break;
        case STATELESS_NS_UPDATE:
            question = ldns_rr_list_rr(ldns_pkt_question(response), 0);
            assert(question);
            if (ldns_rr_get_type(question) == LDNS_RR_TYPE_SOA) {
                if (ldns_pkt_get_rcode(response) != LDNS_RCODE_NOERROR) {
                    stateless_log_info("NS(%s) Update declined. Switch to passive mode", ns->domain_str);
                    ns->state = STATELESS_NS_PASSIVE;

                    /* Send pending records */
                    update = ldns_update_pkt_new(ldns_rdf_clone(ns->domain_rdf), LDNS_RR_CLASS_IN, NULL, ns->pending, NULL);
                    if (!update) {
                        return STATELESS_STATUS_NOMEM;
                    }

                    c_rr_list = stateless_cache_find_rrset(ns->cache, ns->domain_rdf, LDNS_RR_TYPE_NS);
                    c_rr_list = stateless_cache_find_rrset(ns->cache, ldns_rr_rdf(ldns_rr_list_rr(c_rr_list, 0), 0), LDNS_RR_TYPE_A);
                    addr = ldns_rr_rdf(ldns_rr_list_rr(c_rr_list, 0), 0);
                    return stateless_server_send_query(ns->s, ns, update, addr);
                }

                /* Update main ns. Stay in update mode */
                stateless_log_debug("NS(%s) Send A query to update main ns", ns->domain_str);
                return query_pkt(ns, ns->domain_str, LDNS_RR_TYPE_A, LDNS_RD|LDNS_CD);
            }
            else if (ldns_rr_get_type(question) == LDNS_RR_TYPE_A) {
                /* TODO Check if update succed */
                /* TODO Merge following instructions with probing... (shameless copy) */
                /* Registered as nameserver, Add own ns to cache */
                rr_list = generate_NS_A(ns);
                if (!rr_list) {
                    return STATELESS_STATUS_NOMEM;
                }
                status = stateless_cache_insert_list(ns->cache, rr_list);
                if (status != STATELESS_STATUS_OK) {
                    ldns_pkt_set_rcode(response, LDNS_RCODE_SERVFAIL);
                    return status;
                }
                /* TODO Add pending records to cache */

                ns->state = STATELESS_NS_ACTIVE;
                stateless_log_info("NS(%s) Switch state active", ns->domain_str);
            }
            else {
                stateless_log_debug("NS(%s) Wrong response", ns->domain_str);
            }
            break;
        case STATELESS_NS_PROBING:
            if (ancount) {
                /* Registered as nameserver, Add own ns to cache */
                rr_list = generate_NS_A(ns);
                if (!rr_list) {
                    return STATELESS_STATUS_NOMEM;
                }
                status = stateless_cache_insert_list(ns->cache, rr_list);
                if (status != STATELESS_STATUS_OK) {
                    ldns_pkt_set_rcode(response, LDNS_RCODE_SERVFAIL);
                    return status;
                }

                ns->state = STATELESS_NS_ACTIVE;
                stateless_log_info("NS(%s) Switch state active", ns->domain_str);
                stateless_cache_insert_list(ns->cache, ns->pending);
                ns->pending = NULL;
            }
            else {
                stateless_log_debug("NS(%s) Not a valid probing response %%p", ns->domain_str, response);
            }
            break;
        case STATELESS_NS_ACTIVE:
            stateless_log_warn("NS(%s) Active mode. TODO Should only get update responses", ns->domain_str);
            break;
        case STATELESS_NS_PASSIVE:
            ldns_pkt_set_rcode(response, LDNS_RCODE_SERVFAIL);
            stateless_log_debug("NS(%s) Receive a response in passive state", ns->domain_str);
            break;
        default:
            stateless_log_debug("NS(%s) Wrong state", ns->domain_str);
    }

    return STATELESS_STATUS_OK;
}

static ldns_rr* generate_A(const ldns_rdf* owner, const char* ip, uint32_t ttl) {
    ldns_rdf* data;

    assert(owner);
    assert(ip);

    data = ldns_rdf_new_frm_str(LDNS_RDF_TYPE_A, ip);
    if (!data) {
        return NULL;
    }
    return generate_rr(LDNS_RR_TYPE_A, ldns_rdf_clone(owner), data, ttl);
}

static StatelessStatus cache_lookup_query(const StatelessCache* sc, const ldns_rdf* owner, ldns_rr_type type, ldns_pkt* pkt, ldns_pkt_section section) {
    const ldns_rr_list* crrl;
    ldns_rr_list* rrl;
    const ldns_rr* crr;
    size_t i;

    assert(sc);
    assert(owner);
    assert(pkt);

    stateless_log_debug("Cachelookup (type%d): %%d", type, owner);

    crrl = stateless_cache_find_rrset(sc, owner, type);
    if (!crrl) {
        ldns_pkt_set_rcode(pkt, LDNS_RCODE_NXDOMAIN);
    }
    else {
        rrl = ldns_rr_list_clone(crrl);
        if (!rrl) {
            return STATELESS_STATUS_NOMEM;
        }
        else if (!ldns_pkt_push_rr_list(pkt, section, rrl)) {
            ldns_rr_list_deep_free(rrl);
            return STATELESS_STATUS_ADD_RRS;
        }

        /* Add additional information for NS queries (TODO CNAME/DNAME?) */
        if (type == LDNS_RR_TYPE_NS) {
            for (i = 0; i < ldns_rr_list_rr_count(crrl); ++i) {
                crr = ldns_rr_list_rr(crrl, i);
                owner = ldns_rr_rdf(crr, 0);
                type = LDNS_RR_TYPE_A;
                /* Search recusive, but ignore errors (It exist an already successfull lookup) */
                (void)cache_lookup_query(sc, owner, type, pkt, LDNS_SECTION_ADDITIONAL);
            }
        }

        /* Found at least one answer. (overwrite evtl fail rcode from recursive calls) */
        ldns_pkt_set_rcode(pkt, LDNS_RCODE_NOERROR);
    }

    return STATELESS_STATUS_OK;
}

static StatelessStatus generate_update_response(const StatelessNameserver* ns, ldns_pkt* pkt) {
    ldns_rr* rr;

    assert(ns);
    assert(pkt);
    stateless_log_debug("NS(%s) Generate update response", ns->domain_str);

    /* Generate the A query with ttl 0 */
    rr = generate_rr(LDNS_RR_TYPE_A, ldns_rdf_clone(ns->domain_rdf), ldns_rdf_clone(ns->address_rdf), 0);
    if (!rr) {
        return STATELESS_STATUS_NOMEM;
    }
    ldns_rr_set_ttl(rr, 0);
    if (!ldns_pkt_push_rr(pkt, LDNS_SECTION_ANSWER, rr)) {
        stateless_log_debug("Can't add rr to %%p", pkt);
        ldns_rr_free(rr);
        return STATELESS_STATUS_ADD_RRS;
    }

    /* Append known nameservers to query */
    return cache_lookup_query(ns->cache, ns->domain_rdf, LDNS_RR_TYPE_NS, pkt, LDNS_SECTION_AUTHORITY);
}

static StatelessStatus handle_update(StatelessNameserver* ns, ldns_pkt* pkt, const struct sockaddr_storage* css) {
    StatelessStatus status = STATELESS_STATUS_OK;
    ldns_rr_list* update;
    ldns_pkt_rcode rcode = LDNS_RCODE_NOERROR;

    assert(ns);
    assert(css);
    assert(pkt);

    /* Extract update list */
    update = ldns_pkt_authority(pkt);
    ldns_pkt_set_nscount(pkt, 0);
    ldns_pkt_set_authority(pkt, NULL);

    /* Currently ignore NS updates */
    if (ldns_rr_get_type(ldns_rr_list_rr(update, 0)) == LDNS_RR_TYPE_NS) {
        stateless_log_info("Refuse update: %%l", update);
        rcode = LDNS_RCODE_REFUSED;
        ldns_rr_list_deep_free(update);
    }
    else {
        /* Add update to cache */
        status = stateless_cache_insert_list(ns->cache, update);
        if (status != STATELESS_STATUS_OK) {
            rcode = LDNS_RCODE_SERVFAIL;
            ldns_rr_list_deep_free(update);
        }
    }

    /* Send acceptance back */
    ldns_pkt_set_rcode(pkt, rcode);
    status = stateless_server_send(ns->s, pkt, css);

    return STATELESS_STATUS_OK;
}

/* Currently generates a dummy SOA */
static ldns_rr* generate_SOA(StatelessNameserver* ns) {
    ldns_rr* rr;
    char buffer[1024]; //TODO Get max soa string length
    snprintf(buffer, 1024, "%s IN SOA %s root.%s (1 2 3 4 5)", ns->domain_str, ns->domain_str, ns->domain_str);
    (void)ldns_rr_new_frm_str(&rr, buffer, 0, NULL, NULL);
    return rr;
}

StatelessStatus stateless_nameserver_handle_query(StatelessNameserver* ns, ldns_pkt* query_response, StatelessMethod method, const char* data, const struct sockaddr_storage* css) {

    ldns_rr* question;
    ldns_rr* rr = NULL;
    ldns_rr_type type;
    ldns_rdf* owner;
    StatelessStatus status = STATELESS_STATUS_OK;

    assert(ns);
    assert(query_response);
    assert(ldns_pkt_qdcount(query_response) == 1);
    assert(css);

    /* Set response bits */
    ldns_pkt_set_qr(query_response, true);
    ldns_pkt_set_aa(query_response, true);
    ldns_pkt_set_rd(query_response, false);

    question = ldns_rr_list_rr(ldns_pkt_question(query_response), 0);
    owner = ldns_rr_owner(question);
    type = ldns_rr_get_type(question);

    switch (ns->state) {
        case STATELESS_NS_INITIALIZE:
            /* Should be only possible, if current ip has ungracefully leave the network */
            ldns_pkt_set_rcode(query_response, LDNS_RCODE_NXDOMAIN);
            break;

        case STATELESS_NS_ACTIVE:
            /* Handle update queries */
            if (ldns_pkt_get_opcode(query_response) == LDNS_PACKET_UPDATE) {
                if (data) {
                    ldns_pkt_set_rcode(query_response, LDNS_RCODE_NOTAUTH);
                    break;
                }
                return handle_update(ns, query_response, css);
            }
            if (!data && type == LDNS_RR_TYPE_A) {
                /* Queries to the nameserver itself == Update query */
                status = generate_update_response(ns, query_response);
            }
            else if (!data && type == LDNS_RR_TYPE_SOA) {
                /* Answer with dummy soa */
                rr = generate_SOA(ns);
                stateless_log_info("NS(%s) Response with dummy SOA: %%r", ns->domain_str, rr);
                ldns_pkt_push_rr(query_response, LDNS_SECTION_ANSWER, rr);
                status = STATELESS_STATUS_OK;
            }
            else {
                /* Receive a normal query in the correct state --> cache lookup */
                status = cache_lookup_query(ns->cache, owner, type, query_response, LDNS_SECTION_ANSWER);
            }
            break;

        case STATELESS_NS_PROBING:
            if (method == STATELESS_METHOD_T) {
                rr = generate_A(owner, "1.2.3.4", 0);
                if (!rr) {
                    status = STATELESS_STATUS_NOMEM;
                }
                else if (!ldns_pkt_push_rr(query_response, LDNS_SECTION_ANSWER, rr)) {
                    ldns_rr_free(rr);
                    status = STATELESS_STATUS_ADD_RRS;
                }
                break;
            }
            /* else fall through unsupported */
        case STATELESS_NS_UPDATE:
        case STATELESS_NS_PASSIVE:
            /* Incosistent server state */
            ldns_pkt_set_rcode(query_response, LDNS_RCODE_SERVFAIL);
    }

    if (status == STATELESS_STATUS_OK) {
        stateless_log_debug("NS(%s) Response: %%p", ns->domain_str, query_response);

        /* send response */
        return stateless_server_send(ns->s, query_response, css);
    }
    return status;
}

StatelessStatus stateless_nameserver_add_record(StatelessNameserver* ns, ldns_rr* rr) {
    StatelessStatus ss = STATELESS_STATUS_OK;
    ldns_pkt* update;
    ldns_rr_list* rr_list;
    const ldns_rr_list* c_rr_list;
    ldns_rdf* addr;

    assert(ns);
    assert(rr);

    switch(ns->state) {
        case STATELESS_NS_ACTIVE:
            /* Insert in cache */
            ss = stateless_cache_insert(ns->cache, rr);
            if (ss == STATELESS_STATUS_OK) {
                stateless_log_debug("(%s, %s) insert: %%r", ns->domain_str, ns->address_str, rr);
            }
            break;
        case STATELESS_NS_PASSIVE:
            /* Send update */
            rr_list = ldns_rr_list_new();
            if (!rr_list || !ldns_rr_list_push_rr(rr_list, rr)) {
                ss = STATELESS_STATUS_NOMEM;
            }
            else {
                update = ldns_update_pkt_new(ldns_rdf_clone(ns->domain_rdf), LDNS_RR_CLASS_IN, NULL, rr_list, NULL);
                if (!update) {
                    ldns_rr_list_free(rr_list);
                    ldns_rr_list_deep_free(rr_list);
                    return STATELESS_STATUS_NOMEM;
                }
                c_rr_list = stateless_cache_find_rrset(ns->cache, ns->domain_rdf, LDNS_RR_TYPE_NS);
                c_rr_list = stateless_cache_find_rrset(ns->cache, ldns_rr_rdf(ldns_rr_list_rr(c_rr_list, 0), 0), LDNS_RR_TYPE_A);
                addr = ldns_rr_rdf(ldns_rr_list_rr(c_rr_list, 0), 0);
                ss = stateless_server_send_query(ns->s, ns, update, addr);
            }

            ss = STATELESS_STATUS_TODO;
            break;
        case STATELESS_NS_INITIALIZE:
        case STATELESS_NS_UPDATE:
        case STATELESS_NS_PROBING:
            /* queue as pending */
            if (!ldns_rr_list_push_rr(ns->pending, rr)) {
                ss = STATELESS_STATUS_NOMEM; // TODO: check errortype
            }
            else {
                stateless_log_debug("(%s, %s) pending: %%r", ns->domain_str, ns->address_str, rr);
            }
            break;
        default:
            ss = STATELESS_STATUS_ERROR;
    }

    return ss;
}
