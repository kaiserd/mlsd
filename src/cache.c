#include <ldns/ldns.h>

#include <glib.h>

#include "internal.h"
#include "status.h"
#include "nameserver.h"
#include "cache.h"
#include "timespec.h"

typedef struct CacheEntry {
    StatelessTime t;
    ldns_rr_list* list;
} CacheEntry;

typedef struct CacheOwner {
    ldns_rbtree_t* rb_entry;
} CacheOwner;

struct StatelessCache {
    const StatelessNameserver* ns;
    ldns_rdf* ns_rdf;
    ldns_radix_t *radix_owner;
};

/* CacheEntry
 * **********/
static CacheEntry* cache_entry_new(void) {
    CacheEntry* ce;

    ce = malloc(sizeof(CacheEntry));
    if (!ce) {
        return NULL;
    }

    ce->list = ldns_rr_list_new();
    if (!ce->list) {
        free(ce);
        return NULL;
    }

    if (stateless_time_get(&ce->t) != STATELESS_STATUS_OK) {
        ldns_rr_list_free(ce->list);
        free(ce);
        return NULL;
    }
    return ce;
}

/* Returns -1 if ttl expired, else 0 */
static int cache_entry_update_time_rr(ldns_rr* rr, int diff_sec) {
    int ttl;
    int ret = 0;

    assert(rr);

    ttl = ldns_rr_ttl(rr) - diff_sec;
    if (ttl <= 0) {
        ttl = 0;
        ret = -1;
    }

    ldns_rr_set_ttl(rr, ttl);
    return ret;
}

static StatelessStatus cache_entry_update_time(CacheEntry* ce) {
    int64_t diff_nano;
    int diff_sec;
    StatelessTime new;
    StatelessStatus s = STATELESS_STATUS_OK;
    size_t i;

    assert(ce);

    s = stateless_time_get(&new);
    if (s == STATELESS_STATUS_OK) {
        diff_nano = stateless_time_diff(&ce->t, &new);
        assert(diff_nano >= 0);

        diff_sec = diff_nano / STATELESS_NANO_PER_SEC;
        if (diff_sec > 0) {
            for (i = 0; i < ldns_rr_list_rr_count(ce->list); ++i) {
                if(cache_entry_update_time_rr(ldns_rr_list_rr(ce->list, i), diff_sec)) {
                    /* TODO Remove from cache (silently?) */
                    stateless_log_warn("ttl <= 0");
                }
            }
        }
        ce->t = new;
    }
    return s;
}

static void cache_entry_free(CacheEntry** ce) {
    assert(ce);
    assert(*ce);

    ldns_rr_list_free((*ce)->list);
    free(*ce);
    *ce = NULL;
}

/* CacheOwner
 * **********/
static int compare_rr_types(const void* a, const void* b) {
    if ((ldns_rr_type)a < (ldns_rr_type)b)
        return -1;
    if ((ldns_rr_type)a > (ldns_rr_type)b)
        return 1;
    return 0;
}

static CacheOwner* cache_owner_new(void) {
    CacheOwner* co;

    co = malloc(sizeof(CacheOwner));
    if (!co) {
        return NULL;
    }

    co->rb_entry = ldns_rbtree_create(compare_rr_types);
    if (!co->rb_entry) {
        free(co);
        return NULL;
    }

    return co;
}

static void cache_owner_free(CacheOwner** co) {
    assert(co);
    assert(*co);

    ldns_rbtree_free((*co)->rb_entry);
    free(*co);
    *co = NULL;
}

/* Cache
 * *****/
StatelessCache* stateless_cache_new(const StatelessNameserver* ns) {
    StatelessCache* sc;

    assert(ns);

    sc = malloc(sizeof(StatelessCache));
    if (!sc) {
        return NULL;
    }

    sc->ns_rdf = ldns_rdf_new_frm_str(LDNS_RDF_TYPE_DNAME, stateless_nameserver_domain(ns));
    if (!sc->ns_rdf) {
        free(sc);
        return NULL;
    }

    sc->radix_owner = ldns_radix_create();
    if (!sc->radix_owner) {
        ldns_rdf_free(sc->ns_rdf);
        free(sc);
        return NULL;
    }

    sc->ns = ns;
    return sc;
}

void stateless_cache_free(StatelessCache** sc) {
    assert(*sc);
    free(*sc);
    *sc = NULL;
}

void stateless_cache_depp_free(StatelessCache** sc) {
    assert(sc);
    assert(*sc);
}

/* generate radix key (without domain(always equal) and reversed(better radix usage))
 * Returns length of key */
static int owner2key(uint8_t* buffer, const ldns_rdf* owner, const ldns_rdf* domain) {
    uint8_t* data;
    int i, l;

    assert(buffer);
    assert(owner);
    assert(domain);

    /* Copy needed data reserved */
    data = ldns_rdf_data(owner);
    l = ldns_rdf_size(owner) - ldns_rdf_size(domain) + 1;
    /* fprintf(stderr, "key: '"); */
    for(i = 0; i < l; i++) {
        buffer[i] = tolower(data[l-i-1]);
        /* fprintf(stderr, "%d", buffer[i]); */
    }
    /* fprintf(stderr, "'\n"); */
    return l;
}

StatelessStatus stateless_cache_insert(StatelessCache* sc, ldns_rr* rr) {
    uint8_t key[LDNS_MAX_RDFLEN];
    int len;
    ldns_rdf* owner;
    ldns_radix_node_t* node_radix;
    ldns_rbnode_t* node_rb;
    ldns_rr_type type;
    ldns_status status;
    CacheOwner* co;
    CacheEntry* ce;
    StatelessStatus s;

    assert(sc);
    assert(rr);

    /* Init radix key */
    owner = ldns_rr_owner(rr);
    assert(ldns_rdf_get_type(owner) == LDNS_RDF_TYPE_DNAME);
    len = owner2key(key, owner, sc->ns_rdf);

    /* Search existing owner-node */
    node_radix = ldns_radix_search(sc->radix_owner, key, len);
    if (!node_radix) {
        /* stateless_log_debug("Cache: Generate new owner node '%.*s'", len, key); */
        /* Generate new rr-type cache for owner */
        co = cache_owner_new();
        if (!co) {
            return STATELESS_STATUS_NOMEM;
        }
        status = ldns_radix_insert(sc->radix_owner, key, len, co);
        if (status != LDNS_STATUS_OK) {
            cache_owner_free(&co);
            return STATELESS_STATUS_CACHE_INSERT;
        }
    }
    else {
        co = node_radix->data;
    }

    /* Search type */
    type = ldns_rr_get_type(rr);
    node_rb = ldns_rbtree_search(co->rb_entry, (void*)type);
    if (!node_rb) {
        /* stateless_log_debug("Cache: Generate new rr-type node '%d'", type); */

        /* Generate and insert rr-list */
        ce = cache_entry_new();
        if (!ce) {
            // TODO remove radix node if empty?
            return STATELESS_STATUS_NOMEM;
        }
        node_rb = LDNS_MALLOC(ldns_rbnode_t);
        if (!node_rb) {
            cache_entry_free(&ce);
            return STATELESS_STATUS_NOMEM;
        }

        node_rb->key = (void*)type;
        node_rb->data = ce;
        if (ldns_rbtree_insert(co->rb_entry, node_rb) == NULL) {
            LDNS_FREE(node_rb);
            cache_entry_free(&ce);
            return STATELESS_STATUS_CACHE_INSERT;
        }
    }
    else {
        ce = (CacheEntry*)node_rb->data;
        /* TODO Clean if empty after update? */
        s = cache_entry_update_time(ce);
        if (s != STATELESS_STATUS_OK) {
            return s;
        }
    }

    /* Insert if not already exist */
    if (ldns_rr_list_contains_rr(ce->list, rr)) {
        stateless_log_warn("Cache: Record '%%r' already exits", rr);
    }
    if(!ldns_rr_list_push_rr(ce->list, rr)) {
        return STATELESS_STATUS_CACHE_INSERT;
    }
    stateless_log_debug("Cache: Insert '%%r'", rr);
    return STATELESS_STATUS_OK;
}

StatelessStatus stateless_cache_insert_list(StatelessCache* sc, ldns_rr_list* rr_list) {
    StatelessStatus s = STATELESS_STATUS_OK;
    size_t i, j;

    assert(sc);
    assert(rr_list);

    for (i = 0; i < ldns_rr_list_rr_count(rr_list); ++i) {
        s = stateless_cache_insert(sc, ldns_rr_list_rr(rr_list, i));
        if (s != STATELESS_STATUS_OK) {
            for (j = 0; j < i; ++j) {
                stateless_cache_delete(sc, ldns_rr_list_rr(rr_list, i));
            }
            break;
        }
    }
    ldns_rr_list_free(rr_list);
    return s;
}

const ldns_rr* stateless_cache_find(const StatelessCache* sc, ldns_rr* rr) {
    const ldns_rr_list* list;
    ldns_rr* rr_ret;
    size_t i;

    assert(sc);
    assert(rr);

    list = stateless_cache_find_rrset(sc, ldns_rr_owner(rr), ldns_rr_get_type(rr));
    if (list) {
        for (i = 0; i < ldns_rr_list_rr_count(list); ++i) {
            rr_ret = ldns_rr_list_rr(list, i);
            if (!ldns_rr_compare_no_rdata(rr_ret, rr)) {
                return rr_ret;
            }
        }
    }
    return NULL;
}

static ldns_radix_node_t* find_owner_node(const StatelessCache* sc, const ldns_rdf* owner) {
    uint8_t key[LDNS_MAX_RDFLEN];
    int len;

    assert(sc);
    assert(owner);
    assert(ldns_rdf_get_type(owner) == LDNS_RDF_TYPE_DNAME);

    len = owner2key(key, owner, sc->ns_rdf);
    return ldns_radix_search(sc->radix_owner, key, len);
}

const ldns_rr_list* stateless_cache_find_rrset(const StatelessCache* sc, const ldns_rdf* owner, const ldns_rr_type type) {
    ldns_radix_node_t* node_radix;
    ldns_rbnode_t* node_rb;
    CacheEntry* ce;

    assert(sc);
    assert(owner);

    node_radix = find_owner_node(sc, owner);
    if (node_radix) {
        node_rb = ldns_rbtree_search(((CacheOwner*)node_radix->data)->rb_entry, (void*)type);
        if (node_rb) {
            ce = (CacheEntry*)node_rb->data;
            /* TODO time_error / clean? */
            cache_entry_update_time(ce);
            return ce->list;
        }
    }
    return NULL;
}

ldns_rr_list* stateless_cache_find_any(const StatelessCache* sc, const ldns_rdf* owner) {
    ldns_radix_node_t* node_radix;
    ldns_rbnode_t* node_rb;
    ldns_rr_list* rr_list_any;
    CacheEntry* ce;

    assert(sc);
    assert(owner);

    rr_list_any = ldns_rr_list_new();
    if (!rr_list_any) {
        return NULL;
    }

    node_radix = find_owner_node(sc, owner);
    if (node_radix) {
        node_rb = ldns_rbtree_first(((CacheOwner*)node_radix->data)->rb_entry);
        while (node_rb && node_rb != &ldns_rbtree_null_node) {
            ce = (CacheEntry*)node_rb->data;
            /* TODO time_error / clean /push ? */
            cache_entry_update_time(ce);
            ldns_rr_list_push_rr_list(rr_list_any, ce->list);

            node_rb = ldns_rbtree_next(node_rb);
        }
    }

    if(ldns_rr_list_rr_count(rr_list_any) == 0) {
        ldns_rr_list_free(rr_list_any);
        return NULL;
    }
    return rr_list_any;
}

StatelessStatus stateless_cache_delete(StatelessCache* sc, const ldns_rr* rr) {
    assert(sc);
    assert(rr);
    return -1;
}

StatelessStatus stateless_cache_delete_rrset(StatelessCache* sc, const ldns_rdf* owner, const ldns_rr_type type) {
    assert(sc);
    assert(owner);
    (void)type;
    return -1;
}

StatelessStatus stateless_cache_delete_any(StatelessCache* sc, const ldns_rdf* owner) {
    assert(sc);
    assert(owner);
    return -1;
}
