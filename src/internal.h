#ifndef foointernalhfoo
#define foointernalhfoo

#include <regex.h>
#include <time.h>
#include <glib.h> /* TODO get rid of it (alternativ hashtables) */
#include <sys/queue.h>
#include <ldns/ldns.h>

#include "status.h"
#include "logger.h"
#include "stateless.h"

#define STATELESS_DNS_PORT 53
/* #define STATELESS_DNS_SERVER "10.0.5.1" */
#define STATELESS_DNS_SERVER "10.0.10.2"
/* #define STATELESS_DNS_SERVER "127.0.0.1" */
#define STATELESS_ECHO_DOMAIN "echo.local."
#define STATELESS_MAX_ALIAS_DOMAIN 64

#define min(X,Y) (((X) < (Y)) ? (X) : (Y))
#define max(X,Y) (((X) > (Y)) ? (X) : (Y))

typedef GHashTable StatelessPktManager;
typedef struct StatelessPkt StatelessPkt;
typedef struct StatelessNameserver StatelessNameserver;
typedef struct timespec StatelessTime;

typedef enum {
    STATELESS_TYPE_RR_A,
    STATELESS_TYPE_RR_AAAA,
    STATELESS_TYPE_RR_SRV,
    STATELESS_TYPE_RR_PTR,
    STATELESS_TYPE_RR_TXT,
} StatelessTypeRR;

typedef struct StatelessInterface {
    StatelessServer* s;

    char* addr;
    ldns_rdf* rdf;

    struct sockaddr_storage sock_addr;
    size_t sock_len;

    ldns_radix_t* ns;

    LIST_ENTRY(StatelessInterface) entries;
} StatelessInterface;

typedef struct StatelessSocketAddress {
    struct sockaddr_storage addr;
    size_t len;
} StatelessSocketAddress;

/* Intern-only server functions */

/* Send packet to NS if addr==NULL else to given addr */
StatelessStatus stateless_server_send_query(
        StatelessServer* s,
        StatelessNameserver* ns,
        ldns_pkt* pkt,
        const ldns_rdf* addr);

StatelessStatus stateless_server_send(
        StatelessServer* s,
        ldns_pkt* pkt,
        const struct sockaddr_storage* addr
        );

typedef enum {
    STATELESS_METHOD_UNKNOWN,
    STATELESS_METHOD_00,
    STATELESS_METHOD_T
} StatelessMethod;

#endif
