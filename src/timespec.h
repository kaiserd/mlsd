#ifndef footimespechfoo
#define footimespechfoo


#define STATELESS_NANO_PER_SEC 1000000000

/* https://blog.habets.se/2010/09/gettimeofday-should-never-be-used-to-measure-time */
/* typedef struct timespec StatelessTime; */ /* --> internal.h */

StatelessStatus stateless_time_get(StatelessTime* st);

/* return a less/equal/bigger b: -1/0/1 */
int stateless_time_comp(StatelessTime* a, StatelessTime* b);

/* to-from nanoseconds */
int64_t stateless_time_diff(StatelessTime* from, StatelessTime* to);

StatelessStatus stateless_time_stop(StatelessTime* st, uint64_t* diff);


#endif
