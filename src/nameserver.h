#ifndef foonameserverhfoo
#define foonameserverhfoo

StatelessNameserver* stateless_nameserver_new(
        StatelessServer* s,
        const char *domain,
        const char* address);

void stateless_nameserver_free(StatelessNameserver** ns);

const char* stateless_nameserver_domain(const StatelessNameserver* ns);

StatelessStatus stateless_nameserver_response(
        StatelessNameserver* ns,
        ldns_pkt* response);

StatelessStatus stateless_nameserver_handle_query(
        StatelessNameserver* ns,
        ldns_pkt* query_response,
        StatelessMethod method,
        const char* data,
        const struct sockaddr_storage* css
        );

StatelessStatus stateless_nameserver_add_record(
        StatelessNameserver* ns,
        ldns_rr* rr);

#endif
