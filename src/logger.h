#ifndef foologgerh
#define foologgerh

typedef enum {
  STATELESS_LOG_DEBUG = 0,
  STATELESS_LOG_INFO,
  STATELESS_LOG_WARN,
  STATELESS_LOG_ERROR
} StatelessLog;

/* if filename is empty or error open, fallback to stderr */
void stateless_log_init(const char* filename);
void stateless_log_close(void);

/* Set/Get current loglevel */
StatelessLog stateless_log_level(void);
void stateless_log_set_level(StatelessLog l);

/* Log functions */
void stateless_log_debug(const char* fmt, ...);
void stateless_log_info(const char* fmt, ...);
void stateless_log_warn(const char* fmt, ...);
void stateless_log_error(const char* fmt, ...);

#endif
