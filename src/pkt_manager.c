#include <stdlib.h>

#include <glib.h>
#include <ldns/ldns.h>

#include "internal.h"
#include "status.h"
#include "nameserver.h"
#include "timespec.h"
#include "pkt_manager.h"


typedef enum {
    STATELESS_PKT_PENDING,
    STATELESS_PKT_SEND
} StatelessPktState;

struct StatelessPkt {
    StatelessNameserver *ns;
    ldns_pkt* pkt;
    StatelessPktState state;
    unsigned int n_try;
    StatelessTime last_send;
};


StatelessPktManager* stateless_pkt_manager_new(void) {
    srand(time(NULL));
    return g_hash_table_new(g_int_hash, g_int_equal);
}

void stateless_pkt_manager_free(StatelessPktManager** spm) {
    g_hash_table_destroy(*spm);
    *spm = NULL;
}

static void pkt_manager_insert(StatelessPktManager* spm, StatelessPkt* pkt) {
    int* id;

    /* TODO error handling */
    id = malloc(sizeof(int));
    /* Generate unique pkt id */
    *id = rand() % (1<<16);
    while (g_hash_table_contains(spm, id)) {
        /* Normaly very little packets in table, so the brute force should be save */
        *id = rand() % (1<<16);
    }
    ldns_pkt_set_id(pkt->pkt, *id);

    /* TODO error handling? */
    (void)g_hash_table_insert(spm, id, pkt);
}

StatelessPkt* stateless_pkt_find(StatelessPktManager* spm, const ldns_pkt* response) {
    int id;
    StatelessPkt* query;

    assert(spm);
    assert(response);

    id = ldns_pkt_id(response);
    query = g_hash_table_lookup(spm, &id);
    if (query) {
        /* Validiy checks */
        if (ldns_pkt_qdcount(response) != 1) {
            stateless_log_error("Wrong question amount");
            return NULL;
        }
        if (ldns_rr_list_compare(ldns_pkt_question(query->pkt), ldns_pkt_question(response))) {
            stateless_log_error("Response to wrong query");
            return NULL;
        }
        return query;
    }

    return NULL;
}

void stateless_pkt_free(StatelessPktManager* spm, StatelessPkt** pkt) {
    int* key;
    StatelessPkt* value;
    int id;

    assert(spm);
    assert(pkt);
    assert(*pkt);

    id = ldns_pkt_id((*pkt)->pkt);

    /* Lookup */
    if (g_hash_table_lookup_extended(spm, &id, (void**)&key, (void**)&value)) {
        g_hash_table_remove(spm, &id);
        free(key);

        assert(value == *pkt);
    }

    ldns_pkt_free((*pkt)->pkt);
    free(*pkt);
    *pkt = NULL;
}

static StatelessPkt* pkt_new(StatelessNameserver* ns) {
    StatelessPkt* pkt;

    pkt = malloc(sizeof(StatelessPkt));
    if (!pkt) {
        return NULL;
    }
    pkt->ns = ns;
    pkt->state = STATELESS_PKT_PENDING;
    pkt->n_try = 0;
    pkt->last_send.tv_sec = 0;
    pkt->last_send.tv_nsec = 0;
    pkt->pkt = NULL;

    return pkt;
}

StatelessPkt* stateless_pkt_add(StatelessPktManager* spm, StatelessNameserver* ns, ldns_pkt* pkt) {
    StatelessPkt* stateless_pkt;

    assert(spm);
    assert(ns);
    assert(pkt);

    stateless_pkt = pkt_new(ns);
    if (!stateless_pkt) {
        return NULL;
    }

    stateless_pkt->pkt = pkt;
    pkt_manager_insert(spm, stateless_pkt);
    return stateless_pkt;
}

StatelessStatus stateless_pkt_update_send(StatelessPkt* pkt) {
    assert(pkt);

    pkt->n_try++;
    pkt->state = STATELESS_PKT_SEND;
    return stateless_time_get(&pkt->last_send);
}

StatelessNameserver* stateless_pkt_ns(StatelessPkt* pkt) {
    return pkt->ns;
}
