#include <stdint.h>
#include <assert.h>

#include "internal.h"
#include "status.h"
#include "timespec.h"

StatelessStatus stateless_time_get(StatelessTime* st) {
    assert(st);

    if (clock_gettime(CLOCK_MONOTONIC, st)) {
        return STATELESS_STATUS_TIME;
    }
    return STATELESS_STATUS_OK;
}

int stateless_time_comp(StatelessTime* a, StatelessTime* b) {
    assert(a);
    assert(b);

    if (a->tv_sec < b->tv_sec)
        return -1;
    if (a->tv_sec > b->tv_sec)
        return 1;
    if (a->tv_nsec < b->tv_nsec)
        return -1;
    if (a->tv_nsec > b->tv_nsec)
        return 1;

    return 0;
}

static int64_t stateless_time_diff_intern(StatelessTime* from, StatelessTime* to) {
    assert(from);
    assert(to);
    assert(stateless_time_comp(from, to) <= 0);

    return ((int64_t)to->tv_sec - from->tv_sec)*STATELESS_NANO_PER_SEC + to->tv_nsec - from->tv_nsec;
}

int64_t stateless_time_diff(StatelessTime* from, StatelessTime* to) {
    assert(from);
    assert(to);

    if (stateless_time_comp(from, to) > 0)
        return -stateless_time_diff_intern(to, from);
    return stateless_time_diff_intern(from, to);
}

StatelessStatus stateless_time_stop(StatelessTime* st, uint64_t* diff) {
    int64_t d;
    StatelessTime end;

    assert(st);
    assert(diff);

    if (stateless_time_get(&end)) {
        return STATELESS_STATUS_TIME;
    }

    d = stateless_time_diff(st, &end);
    assert(d >= 0);

    *diff = d;
    return STATELESS_STATUS_OK;
}
