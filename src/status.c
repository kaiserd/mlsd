#include "status.h"
#include "stdio.h"

struct status {
    StatelessStatus s;
    const char* str;
};

static struct status status[] = {
    {STATELESS_STATUS_OK, "All OK"},
    {STATELESS_STATUS_ERROR, "Error"},
    {STATELESS_STATUS_NULL, "null"},
    {STATELESS_STATUS_RR, "rr"},
    {STATELESS_STATUS_IFACE, "iface"},
    {STATELESS_STATUS_TODO, "todo"},
    {STATELESS_STATUS_NOMEM, "Out of memory"},
    {STATELESS_STATUS_ADD_RRS, "rrs"},
    {STATELESS_STATUS_CACHE_INSERT, "Cache insertion failed"},
    {STATELESS_STATUS_CACHE_EXISTS, "Cache entry exists"},
    {STATELESS_STATUS_TIME, "Time"},
    {STATELESS_STATUS_UDP_SEND, "UDP send failed"},
    {STATELESS_STATUS_TCP_SEND, "TCP send failed"},
    {-1, "Unkown status"}
};

const char* stateless_status_str(StatelessStatus s) {
    struct status* p;
    for (p = status; (int)p->s >= 0 && p->s != s; p++);
    return p->str;
}
