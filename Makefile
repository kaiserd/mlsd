VERBOSE := -m64 -std=gnu99 -pedantic -Wall -Wshadow -Wextra
# VERBOSE += -Wpointer-arith -Wcast-qual -Wstrict-prototypes -Wmissing-prototypes

CC      := gcc
CFLAGS  := -g -I src $(shell pkg-config --cflags glib-2.0) -fPIC $(OPTFLAGS) $(VERBOSE)
LDFLAGS := -lldns $(shell pkg-config --libs glib-2.0) -shared $(OPTLIBS)

SOURCES := $(wildcard src/*.c src/**/*.c)
OBJECTS := $(patsubst src/%.c, build/%.o, $(SOURCES))

TARGET=libmlsd.so

.PHONY: all clean examples

all: $(TARGET)

examples: $(TARGET)
	@$(MAKE) -C examples

$(TARGET): build $(OBJECTS) Makefile
	$(CC) $(CFLAGS) $(LDFLAGS) $(OBJECTS) -o $@

build/%.o: src/%.c
	$(CC) $(CFLAGS) -c $< -o $@

build:
	mkdir -p build

clean:
	@rm -rfv build
	@rm -rfv $(TARGET)
	@$(MAKE) -C examples clean
